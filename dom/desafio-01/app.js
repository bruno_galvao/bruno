new Vue ({
    el: "#desafio",
    data: {
        nome: 'Bruno',
        idade: 36,
        linkIMG: 'https://cdn.shopify.com/s/files/1/0533/2089/files/vuejs-tutorial_2d2a853c-aa2f-44b0-80df-933b495f77f8.png?v=1509478492',
    },
    methods: {
        tres () {
            return this.idade * 3 //pode ser assim, ou multiplicar direto no html
        },
        rand () {
            return Math.random([ 0,1 ])
        }
    },
})